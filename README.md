# Go Workshop

# Get started

## Develop

Start docker environment
`docker compose up`

Attach VSCode to the `go-workshop` container

## Build

`go build -o main cmd/main.go`


# Common packages

## Testify 
A framework for assertions and test suites

`github.com/stretchr/testify`

## Chi
A framework/DSL for HTTP webservers

`github.com/go-chi/chi/v5`

## Gin
A framework/DSL for HTTP webservers

`github.com/gin-gonic/gin`



## Database Migrations

`go install -tags 'mysql' github.com/golang-migrate/migrate/v4/cmd/migrate@v4.16.2`

Create migration
`migrate create -ext sql -dir db/migrations -seq create_users_table`

Run migration
`migrate -database mysql://root:letsgo@tcp(go-workshop-mysql:3306)/workshop -path db/migrations up`


### Example

UP

```
BEGIN;

CREATE TABLE  users (
    id BIGINT AUTO_INCREMENT NOT NULL,
    firstname VARCHAR(255)  NOT NULL,
    lastname VARCHAR(255)  NOT NULL,
    age INTEGER  NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

INSERT INTO users(firstname, lastname, age) VALUES
    ('John', 'Doe', 23),
    ('Jane', 'Tail',28),
    ('Max', 'Pritt', 25);

COMMIT;
```

DOWN

```
BEGIN;
DROP TABLE IF EXISTS users;
COMMIT;
```

## SQLX
A wrapper around standard library which includes prepared statements

`go get github.com/jmoiron/sqlx`

## DB Driver
The driver to communicate with your database

`go get github.com/go-sql-driver/mysql`


```
import _ "github.com/go-sql-driver/mysql"
```


# Agenda

1. Hello world, building
2. Types, basics
3. Structs vs Pointers
4. JSON
5. Testing
6. Webserver
7. Database 